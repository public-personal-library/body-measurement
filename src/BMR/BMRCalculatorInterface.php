<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BMR;

/**
 * @package Kematjaya\BodyMeasurement\BMR
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface BMRCalculatorInterface 
{
    public function calculate():float;
}
