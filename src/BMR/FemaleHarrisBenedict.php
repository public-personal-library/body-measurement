<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BMR;

/**
 * @package Kematjaya\BodyMeasurement\BMR
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class FemaleHarrisBenedict extends HarrisBenedict 
{
    public function calculate(): float 
    {
        return round(655 + (9.6 * $this->getWeight())
            + (1.8 * $this->getHeight()) 
            - (4.7 * $this->getAge()), 2);
    }
}
