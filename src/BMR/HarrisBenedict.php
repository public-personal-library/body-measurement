<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BMR;

/**
 * @package Kematjaya\BodyMeasurement\BMR
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class HarrisBenedict implements BMRCalculatorInterface
{
    
    /**
     * 
     * @var float
     */
    private $weight;
    
    /**
     * 
     * @var float
     */
    private $height;
    
    /**
     * 
     * @var float
     */
    private $age;
    /**
     * 
     * @param float $weight (KG)
     * @param float $height (CM)
     * @param float $age count of age
     */
    public function __construct(float $weight, float $height, float $age) 
    {
        $this->weight = $weight;
        $this->height = $height;
        $this->age = $age;
    }
    
    /**
     * 
     * @return float (KG)
     */
    public function getWeight(): float 
    {
        return $this->weight;
    }

    /**
     * 
     * @return float (CM)
     */
    public function getHeight(): float 
    {
        return $this->height;
    }

    /**
     * 
     * @return float
     */
    public function getAge(): float 
    {
        return $this->age;
    }
}
