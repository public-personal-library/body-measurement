<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BMR;

/**
 * @package Kematjaya\BodyMeasurement\BMR
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class MaleHarrisBenedict extends HarrisBenedict
{
    
    public function calculate(): float 
    {
        return round(66 + (13.7 * $this->getWeight())
            + (5 * $this->getHeight()) 
            - (6.8 * $this->getAge()), 2);
    }

}
