<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BodyFat;

/**
 * @package Kematjaya\BodyFatMeasurement\BodyFat
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface BodyFatMeasurementInterface 
{
    public function calculate():float;
}
