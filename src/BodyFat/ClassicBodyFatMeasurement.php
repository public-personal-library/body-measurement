<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BodyFat;

/**
 * Perhitungan IMT dengan perbandingan antara berat badan dengan tinggi badan 
 * @package Kematjaya\BodyFatMeasurement\BodyFat
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ClassicBodyFatMeasurement implements BodyFatMeasurementInterface
{
    /**
     * 
     * @var float (KG)
     */
    private $weight;
    
    /**
     * 
     * @var float (Cm)
     */
    private $height;
    /**
     * 
     * @param float $weight (KG)
     * @param float $height (Cm)
     */
    public function __construct(float $weight, float $height) 
    {
        $this->weight = $weight;
        $this->height = $height;
    }
    
    public function getWeight(): float 
    {
        return $this->weight;
    }

    public function getHeight(): float 
    {
        return $this->height;
    }

    public function calculate(): float 
    {
        $heightInMeter = round($this->getHeight() / 100, 2);
        
        return round($this->getWeight() / ($heightInMeter * $heightInMeter), 2);
    }

}
