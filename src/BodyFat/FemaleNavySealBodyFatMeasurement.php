<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BodyFat;

/**
 * Navy Seal Body Fat Measurement method for Woman
 * @package Kematjaya\BodyMeasurement\BodyFat
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class FemaleNavySealBodyFatMeasurement extends NavySealBodyFatMeasurement
{
    /**
     * 
     * @var float (Cm)
     */
    private $waist;
    
    /**
     * 
     * @var float (Cm)
     */
    private $hip;
    
    /**
     * 
     * @param float $waist (Cm)
     * @param float $hip (Cm)
     * @param float $neck (Cm)
     * @param float $height (Cm)
     */
    public function __construct(float $waist, float $hip, float $neck, float $height) 
    {
        $this->waist = $waist;
        $this->hip = $hip;
        parent::__construct($neck, $height);
    }
    
    /**
     * 
     * @return float (Inches)
     */
    public function getWaist(): float 
    {
        return $this->waist / $this->getConverter();
    }

    /**
     * 
     * @return float (Inches)
     */
    public function getHip(): float 
    {
        return $this->hip / $this->getConverter();
    }

    public function calculate(): float 
    {
        $log = $this->getWaist() + $this->getHip() - $this->getNeck();
        return round(
                (163.205 * log10($log))
                - (97.684 * log10($this->getHeight())) - 78.387
                , 2);
    }

}
