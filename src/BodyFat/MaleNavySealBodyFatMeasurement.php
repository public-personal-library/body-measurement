<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BodyFat;

/**
 * Navy Seal Body Fat Measurement method for Men
 * @package Kematjaya\BodyMeasurement\BodyFat
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class MaleNavySealBodyFatMeasurement extends NavySealBodyFatMeasurement
{
    /**
     * 
     * @var float (Cm)
     */
    private $stomach;
    
    /**
     * 
     * @param float $stomach (Cm)
     * @param float $neck (Cm)
     * @param float $height (Cm)
     */
    public function __construct(float $stomach, float $neck, float $height) 
    {
        $this->stomach = $stomach;
        
        parent::__construct($neck, $height);
    }
    
    /**
     * 
     * @return float (Inches)
     */
    public function getStomach(): float 
    {
        return $this->stomach / $this->getConverter();
    }

    public function calculate(): float 
    {
        return round((86.010 * log10(($this->getStomach() - $this->getNeck()))) 
            - (70.041 * log10($this->getHeight())) + 36.76, 2);
    }

}
