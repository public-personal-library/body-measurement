<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\BodyFat;

/**
 * @package Kematjaya\BodyMeasurement\BodyFat
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class NavySealBodyFatMeasurement implements BodyFatMeasurementInterface
{
    /**
     * 
     * @var float (Cm)
     */
    private $neck;
    
    /**
     * 
     * @var float (Cm)
     */
    private $height;
    
    /**
     * 
     * @param float $neck (Cm)
     * @param float $height (Cm)
     */
    public function __construct(float $neck, float $height) 
    {
        $this->neck = $neck;
        $this->height = $height;
    }
    
    /**
     * 
     * @return float (Inches)
     */
    public function getNeck(): float 
    {
        return $this->neck / $this->getConverter();
    }

    /**
     * 
     * @return float (Inches)
     */
    public function getHeight(): float 
    {
        return $this->height / $this->getConverter();
    }

    protected function getConverter():float
    {
        return 2.54;
    }
}
