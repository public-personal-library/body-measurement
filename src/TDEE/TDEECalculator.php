<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\TDEE;

use Kematjaya\BodyMeasurement\BMR\BMRCalculatorInterface;

/**
 * @package Kematjaya\BodyMeasurement\TDEE
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class TDEECalculator implements TDEEInterface
{
    
    public function calculate(BMRCalculatorInterface $BMRCalculator, float $activityScore): float 
    {
        return round($BMRCalculator->calculate() * $activityScore, 2);
    }

}
