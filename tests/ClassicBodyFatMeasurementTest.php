<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\Tests;

use Kematjaya\BodyMeasurement\BodyFat\ClassicBodyFatMeasurement;

/**
 * @package Kematjaya\BodyFatMeasurement\Tests
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ClassicBodyFatMeasurementTest extends \PHPUnit\Framework\TestCase
{
    public function testCalculate()
    {
        $measurement = new ClassicBodyFatMeasurement(70, 173);
        
        $this->assertEquals(23.39, $measurement->calculate());
    }
}
