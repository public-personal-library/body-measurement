<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\Tests;

use Kematjaya\BodyMeasurement\BodyFat\FemaleNavySealBodyFatMeasurement;

/**
 * @package Kematjaya\BodyMeasurement\Tests
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class FemaleNavySealBodyFatMeasurementTest extends \PHPUnit\Framework\TestCase
{    
    public function testMeasurement()
    {
        $measurement = new FemaleNavySealBodyFatMeasurement(71.12, 81.28, 38.1, 165.1);
        
        $this->assertEquals(14.33, $measurement->calculate());
    }
}
