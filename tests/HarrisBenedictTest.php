<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\Tests;

use Kematjaya\BodyMeasurement\BMR\MaleHarrisBenedict;
use Kematjaya\BodyMeasurement\BMR\FemaleHarrisBenedict;

/**
 * @package Kematjaya\BodyMeasurement\Tests
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class HarrisBenedictTest extends \PHPUnit\Framework\TestCase
{
    public function testMenMeasurement()
    {
        $measurement = new MaleHarrisBenedict(77, 170, 30);
        
        $this->assertEquals(1766.9, $measurement->calculate());
    }
    
    public function testWomenMeasurement()
    {
        $measurement = new FemaleHarrisBenedict(60, 160, 30);
        
        $this->assertEquals(1378, $measurement->calculate());
    }
}
