<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\Tests;

use Kematjaya\BodyMeasurement\BodyFat\MaleNavySealBodyFatMeasurement;

/**
 * @package Kematjaya\BodyMeasurement\Tests
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class MaleNavySealBodyFatMeasurementTest extends \PHPUnit\Framework\TestCase
{
    public function testMeasurement()
    {
        $measurement = new MaleNavySealBodyFatMeasurement(88.9, 45.72, 182.88);
        $this->assertEquals(12.5, $measurement->calculate());
    }
}
