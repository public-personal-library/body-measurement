<?php

/**
 * This file is part of the body-measurement.
 */

namespace Kematjaya\BodyMeasurement\Tests;

use Kematjaya\BodyMeasurement\BMR\MaleHarrisBenedict;
use Kematjaya\BodyMeasurement\TDEE\TDEECalculator;

/**
 * @package Kematjaya\BodyMeasurement\Tests
 * @license https://opensource.org/licenses/MIT MIT
 * @author  Nur Hidayatullah <kematjaya0@gmail.com>
 */
class TDEECalculatorTest extends \PHPUnit\Framework\TestCase
{
    public function testTDEE()
    {
        $BMRCalculator = new MaleHarrisBenedict(60, 160, 30);
        $tdee = new TDEECalculator();
        $this->assertEquals(2611.84, $tdee->calculate($BMRCalculator, 1.76));
    }
}
